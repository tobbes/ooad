var specifications = [

  {
    title: 'Regular Car',
    engine: 'regular',
    weight: 1,
    image: 'car.jpg' 
    }, 

   {title: 'Quick But fat car', 
    engine: 'quick',
    weight: 1.5,
    image: 'fast-car.jpg'} 
];









var Factory = {
  //Creates a car given a type 
  // 0 for  a normal car
  // 1 for a quick but fat car
  makeCar : function(specification) {
    var NewCar = null;
    
    var engine;

    if(specification.engine === 'regular') {
      engine = new Engine('regular',0.5,6);

    }
    else if(specification.engine === 'quick') {
      engine = new Engine('quick',1,5);
    }

    car = new Car(specification.title,engine,specification.weight,specification.image);
     return car;
    } 
    //console.log(NewCar);
   
  };





/* The engine of the car */
function Engine(title,acceleration,maxPower) {
  this.title = title;
  this.acceleration = acceleration;
  this.maxPower = maxPower;
  this.currentPower = 0;
}



/* Carconstructor */
function Car(title,engine,weight,image) {
  this.title = title;
  this.engine = engine;
  this.weight = weight;
  this.image = image;
  this.m = 0;
  this.isAccelerating = false;
}


//starts/stops the car
Car.prototype.changeAcceleration = function () {
  this.isAccelerating = !this.isAccelerating;
}

//moves the car along the track
Car.prototype.update = function() {
    if(this.isAccelerating) {
      //car.currentPower += (car.engine === 'quick' ? 1 : 0.5);
      this.engine.currentPower += this.engine.acceleration;
      if(this.engine.currentPower > this.engine.maxPower) this.engine.currentPower = this.engine.maxPower;
    } 
    else {
      this.engine.currentPower -= this.engine.acceleration;

      if(this.engine.currentPower < 0) this.engine.currentPower = 0;
    }

    var speed = this.engine.currentPower / this.weight;

    this.m += speed;

    console.log(this.title + ": " + this.m);
  }




function RaceTrack(cars) {

  //this starts the cars
  // we want them to be running 
  this.cars = $.map(cars, function (car) {
    
    car.changeAcceleration();
    //car.isAccelerating = true;
    return car;
  });


  this.length = 100;
}

RaceTrack.prototype.tick = function () {
  for(var i = 0, len = this.cars.length; i < len; i++) {
    var car = this.cars[i];
    car.update();
  }
};

	
  //Controller starts the simulation , calls the car to update itself and 
  // calls the view to update itself.	

var Controller = {
  $lapsContainer: $('#laps-container'),
  raceTrack: null,
  init: function (raceTrack) {
    var self = this;

    this.raceTrack = raceTrack;

    // Setup event listeners
    this.setupEventListeners();
    View.init(raceTrack);

    // Setup race loop
    setInterval(function () {
      self.raceTrack.tick();
      View.render();
    }, 100);

    View.render();
  },
  setupEventListeners: function () {
    var self = this;

    this.$lapsContainer.on('click', '.start-stop', function () {
      
      console.log("hej hej hej");
      var $span = $(this);
      var index = $span.data('index');
      var car = self.raceTrack.cars[index];
      car.changeAcceleration();
    });
  }
};



/*The.... view */

var View = {
              // Retrieve the elements needed from the html 
              $renderContainer: $('#render-container'),
              $lapsContainer: $('#laps-container'),
              raceTrack: null,
              previousLapsHTML: null,
            init: function(raceTrack) {

              this.raceTrack = raceTrack;
            },

  /* Renders the cars on screen */          
  render: function() {
    var self = this;

    // Create HTML for stats
    var lapsHTML = $.map(this.raceTrack.cars, function (car, index) {
      var laps = parseInt(car.m / self.raceTrack.length);

      return `${car.title}: ${laps} <button class="btn btn-default start-stop" type="submit" data-index="${index}">Start/Stop</button>`;
    }).join('<br/>');

    // Create HTML for cars
    var carsHTML = $.map(this.raceTrack.cars, function (car, index) {
      //function getCarPosition(m) {
        // Neglect any previous laps already driven
        var percentMade = car.m % self.raceTrack.length;
        var pixelsDriven = percentMade / 100 * 2000;

        // Lane measurements
        // 700 px width
        // 300 px height

        // Adjust for top lane
        var x = Math.max(Math.min(pixelsDriven, 700), 0);
        // Adjust for first turn
        var y = Math.max(Math.min(pixelsDriven - 700, 300), 0);
        // Adjust for second top lane
        x -= Math.max(Math.min(pixelsDriven - 1000, 700), 0);
        // Adjust for second turn
        y -= Math.max(Math.min(pixelsDriven - 1700, 300), 0);
     return `<img src="images/${car.image}" style="left: ${x}px; top: ${y}px;" class="car" data-index="${index}" />`;
    }).join('');

    this.$renderContainer.html(carsHTML);
     if(this.previousLapsHTML !== lapsHTML) {
      this.$lapsContainer.html(lapsHTML);
      this.previousLapsHTML = lapsHTML;
    }
  }

};


// A main class to start things off
// creates cars, racetrack and starts the simulation
// through the controller
function Main() {
      var cars = $.map (specifications,Factory.makeCar);
  var raceTrack = new RaceTrack(cars);

  Controller.init(raceTrack);
};

// Initiate everything as soon as everything has loaded
$(document).ready(function () {
  var main = new Main();
});
